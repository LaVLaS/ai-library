# Flake Analysis

Flake analysis is used to identify false positive in test failures. The model uses DBSCAN clustering algorithm to cluster false positives and then predict the chances of a given failure being a false positive.

## Contents

`flakes_prediction.py` - Compute the probability a given test failure is false positive using the above trained model.
##### Parameters
* s3endpointUrl - Endpoint to s3 storage backend
* s3accessKey - Access key to s3 storage backend
* s3secretKey - Secret key to s3 storage backend
* s3objectStoreLocation - Bucket name in s3 backend
* model - object key including model name (same as s3Destination above unless you relocate the model).
* s3Path - object key containing test failures to run prediction on (location in the bucket)
* s3Destination - location to store the prediction results

## Workflow

### Save Data

#### Prediction Data

The following example shows sample s3Path that points to the folder where prediction data is stored.

    s3Path - flake_analysis/prediction-data/failures/records
    Files -
        testfailure1.json
        testfailure2.json
        testfailure3.json
        ..

The following example shows what an individual training data looks like. 

    testfailure1.json
    {
        "id":"e54bb558-f134-4356-b2f0-59c5cdf44ed3",
        "label":"failure2.txt",
        "log":"# testBasic (check_journal.TestJournal)\n#\n[0905/212816.673168:ERROR:gpu_process_transport_factory.cc(1017)] Lost UI shared context.\n\nDevTools listening on ws://127.0.0.1:9678/devtools/browser/faa4899a-aec5-4a63-8717-9c991913b26b\n[0905/212816.716594:ERROR:zygote_host_impl_linux.cc(267)] Failed to adjust OOM score of renderer with pid 42364: Permission denied (13)\n[0905/212816.874362:ERROR:zygote_host_impl_linux.cc(267)] Failed to adjust OOM score of renderer with pid 42409: Permission denied (13)\nWarning: Stopping systemd-journald.service, but it can still be activated by:\n  systemd-journald-audit.socket\n  systemd-journald.socket\n  systemd-journald-dev-log.socket\n> warning: transport closed: disconnected\nssh_exchange_identification: read: Connection reset by peer\nCDP: {\"source\":\"network\",\"level\":\"error\",\"text\":\"Failed to load resource: net::ERR_EMPTY_RESPONSE\",\"timestamp\":1536182906007.98,\"url\":\"http://127.0.0.2:9591/cockpit/static/fonts/OpenSans-Light-webfont.woff\",\"networkRequestId\":\"42409.28\"}\nssh_exchange_identification: read: Connection reset by peer\n> log: transport closed, dropped message:  1:2!6\n{\"call\":[\"/org/freedesktop/timedate1\",\"org.freedesktop.DBus.Properties\",\"Get\",[\"org.freedesktop.timedate1\",\"NTPSynchronized\"]],\"id\":\"4\"}\n> log: transport closed, dropped message:  1:2!6\n{\"call\":[\"/org/freedesktop/timedate1\",\"org.freedesktop.DBus.Properties\",\"Get\",[\"org.freedesktop.timedate1\",\"NTPSynchronized\"]],\"id\":\"5\"}\nssh_exchange_identification: read: Connection reset by peer\nssh_exchange_identification: read: Connection reset by peer\nssh_exchange_identification: read: Connection reset by peer\nssh_exchange_identification: read: Connection reset by peer\n> log: transport closed, dropped message:  1:2!6\n{\"call\":[\"/org/freedesktop/timedate1\",\"org.freedesktop.DBus.Properties\",\"Get\",[\"org.freedesktop.timedate1\",\"NTPSynchronized\"]],\"id\":\"6\"}\nok 42 testBasic (check_journal.TestJournal) # duration: 43s\n"
    }

### Run Model

#### Prediction

    curl -v --header "Authorization: Bearer $TOKEN" \
    http://seldon-core-apiserver-panbalag.apps.cluster-dc86.dc86.openshiftworkshop.com/api/v0.1/predictions -d \
    '{"strData":"s3endpointUrl='"$S3ENDPOINTURL"', s3accessKey='"$S3ACCESSKEY"', \
      s3secretKey='"$S3SECRETKEY"', s3objectStoreLocation=DH-DEV-DATA, \
      model=flakes/models/testflakes.model, s3Path=flakes/prediction-data/failures/records, \
      s3Destination=flakes/predictions"}' \
      -H "Content-Type: application/json"

### Use Results

#### Prediction

The following example shows sample s3Destination that points to the folder where prediction results are stored.

    s3Destination - flake_analysis/predictions
    Files -
        testfailure1.json
        testfailure2.json
        testfailure3.json
        ..

The following example shows what an individual prediction result looks like. The field 'flake' shows the probability of the given test failure being a false positive.

    testfailure1.json
    {
        "id":"e54bb558-f134-4356-b2f0-59c5cdf44ed3",
        "label":"failure2.txt",
        "log":"# testBasic (check_journal.TestJournal)\n#\n[0905/212816.673168:ERROR:gpu_process_transport_factory.cc(1017)] Lost UI shared context.\n\nDevTools listening on ws://127.0.0.1:9678/devtools/browser/faa4899a-aec5-4a63-8717-9c991913b26b\n[0905/212816.716594:ERROR:zygote_host_impl_linux.cc(267)] Failed to adjust OOM score of renderer with pid 42364: Permission denied (13)\n[0905/212816.874362:ERROR:zygote_host_impl_linux.cc(267)] Failed to adjust OOM score of renderer with pid 42409: Permission denied (13)\nWarning: Stopping systemd-journald.service, but it can still be activated by:\n  systemd-journald-audit.socket\n  systemd-journald.socket\n  systemd-journald-dev-log.socket\n> warning: transport closed: disconnected\nssh_exchange_identification: read: Connection reset by peer\nCDP: {\"source\":\"network\",\"level\":\"error\",\"text\":\"Failed to load resource: net::ERR_EMPTY_RESPONSE\",\"timestamp\":1536182906007.98,\"url\":\"http://127.0.0.2:9591/cockpit/static/fonts/OpenSans-Light-webfont.woff\",\"networkRequestId\":\"42409.28\"}\nssh_exchange_identification: read: Connection reset by peer\n> log: transport closed, dropped message:  1:2!6\n{\"call\":[\"/org/freedesktop/timedate1\",\"org.freedesktop.DBus.Properties\",\"Get\",[\"org.freedesktop.timedate1\",\"NTPSynchronized\"]],\"id\":\"4\"}\n> log: transport closed, dropped message:  1:2!6\n{\"call\":[\"/org/freedesktop/timedate1\",\"org.freedesktop.DBus.Properties\",\"Get\",[\"org.freedesktop.timedate1\",\"NTPSynchronized\"]],\"id\":\"5\"}\nssh_exchange_identification: read: Connection reset by peer\nssh_exchange_identification: read: Connection reset by peer\nssh_exchange_identification: read: Connection reset by peer\nssh_exchange_identification: read: Connection reset by peer\n> log: transport closed, dropped message:  1:2!6\n{\"call\":[\"/org/freedesktop/timedate1\",\"org.freedesktop.DBus.Properties\",\"Get\",[\"org.freedesktop.timedate1\",\"NTPSynchronized\"]],\"id\":\"6\"}\nok 42 testBasic (check_journal.TestJournal) # duration: 43s\n",
        "flake": "0.667"
    }

