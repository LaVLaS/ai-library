# Topic Modeling

Topic modeling is used to summarize content from a document or a URL. The model is based on Latent Dirichlet Allocation.
## Contents

`get_topics.py` - LDA based topic model.
##### Parameters
* s3endpointUrl - Endpoint to s3 storage backend
* s3accessKey - Access key to s3 storage backend
* s3secretKey - Secret key to s3 storage backend
* s3objectStoreLocation - Bucket name in s3 backend
* model - object key including model name (same as s3Destination above unless you relocate the model).
* s3Path - object key containing test failures to run prediction on (location in the bucket)
* s3Destination - location to store the prediction results
* doc - input data (a document that contains text).
* url - url to web content that can be used as input to the model.

## Workflow

### Run Model

#### Prediction

     curl -v --header "Authorization: Bearer $TOKEN" \
     http://seldon-core-apiserver-panbalag.apps.cluster-dc86.dc86.openshiftworkshop.com/api/v0.1/predictions -d \
     '{"strData":"s3endpointUrl='"$S3ENDPOINTURL"', s3accessKey='"$S3ACCESSKEY"', \
      s3secretKey='"$S3SECRETKEY"', s3objectStoreLocation=DH-DEV-DATA, s3Path=topic_model, \
      s3Destination=topic_model, url=, doc=data.txt"}' \
      -H "Content-Type: application/json"

     curl -v --header "Authorization: Bearer $TOKEN" \
     http://seldon-core-apiserver-panbalag.apps.cluster-dc86.dc86.openshiftworkshop.com/api/v0.1/predictions -d \
     '{"strData":"s3endpointUrl='"$S3ENDPOINTURL"', s3accessKey='"$S3ACCESSKEY"', \
     s3secretKey='"$S3SECRETKEY"', s3objectStoreLocation=DH-DEV-DATA, \
     s3Path=topic_model, s3Destination=topic_model, \
     url=https://en.wikipedia.org/wiki/Artificial_intelligence, doc="}' -H "Content-Type: application/json"

