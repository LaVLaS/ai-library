import argparse
import boto3
import botocore
from sklearn.externals import joblib
import pandas as pd
import numpy as np
import json
import os
import inspect
import sys
currentdir = os.path.dirname(
               os.path.abspath(
                inspect.getfile(inspect.currentframe())
                )
               )
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir + "/storage")
import s3

class detect_fraud(object):
 
    def __init__(self):
        print("Initializing")
 
    def predict(self,data,features_names):

        result = "PASS"
        params = dict((item.strip()).split("=") for item in data.split(","))
        print(params)
        eparams = ["s3Path", "s3endpointUrl", "s3objectStoreLocation", "model",
                   "s3accessKey", "s3secretKey", "s3Destination", "inputdata"]
        if not all (x in params for x in eparams):
          print("Not all parameters have been defined")
          result = "FAIL"
          return result

        model = params['model']
        s3Path = params['s3Path']
        s3endpointUrl = params['s3endpointUrl']
        s3objectStoreLocation = params['s3objectStoreLocation']
        s3accessKey = params['s3accessKey']
        s3secretKey = params['s3secretKey']
        s3Destination = params['s3Destination']
        inputdata = params['inputdata']

        # Download the trained model from storage backend in to MODEL_PATH
        session = s3.create_session_and_resource(s3accessKey,
                                                 s3secretKey,
                                                 s3endpointUrl)
        s3.download_file(session,
                         s3objectStoreLocation,
                         s3Path + "/" + model,
                         '/tmp/' + model)

        self.clf = joblib.load('/tmp/' + model)

        #Extract value of X
        dataset = inputdata.split(':')
        dataset = filter(None, dataset)
        featurearray=[float(i) for i in dataset]
        print(featurearray)
        rowdf = pd.DataFrame([featurearray], columns = ['Time','V1','V2','V3','V4', 'V5','V6','Amount'])
        print(rowdf)
        predictions = self.clf.predict(rowdf)
        # initialize list of lists
        print("------------")
        print("RESULT:")
        print(predictions)
        print("------------")
        return predictions

def main():
  parser = argparse.ArgumentParser()
  parser.add_argument('-data', help='prediction data set', default='')
  args = parser.parse_args()
  data = args.data
  obj = detect_fraud()
  obj.predict(data,20)
  
if __name__== "__main__":
  main()

